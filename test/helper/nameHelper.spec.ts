import _ from '../../src/helper'

describe('nameHelper', () => {
  it('split', async () => {
    expect(_.name.split('abc-def-ghi', 'lower')).toEqual(['abc', 'def', 'ghi'])
    expect(_.name.split('abc_def_ghi', 'lower')).toEqual(['abc', 'def', 'ghi'])
    expect(_.name.split('abcDefGhi', 'lower')).toEqual(['abc', 'def', 'ghi'])
    expect(_.name.split('AbcDefGhi', 'lower')).toEqual(['abc', 'def', 'ghi'])
  })

  it('split include number', async () => {
    expect(_.name.split('abc0-de1f-g2hi', 'lower')).toEqual(['abc0', 'de1f', 'g2hi'])
    expect(_.name.split('abc0_de1f_g2hi', 'lower')).toEqual(['abc0', 'de1f', 'g2hi'])
    expect(_.name.split('abc0De1fG2hi', 'lower')).toEqual(['abc0', 'de1f', 'g2hi'])
    expect(_.name.split('Abc0De1fG2hi', 'lower')).toEqual(['abc0', 'de1f', 'g2hi'])
  })

  it('split one word', async () => {
    expect(_.name.split('ABC', 'lower')).toEqual(['abc'])
    expect(_.name.split('abc', 'lower')).toEqual(['abc'])
  })

  it('split type lowor', async () => {
    expect(_.name.split('abc-def-ghi', 'lower')).toEqual(['abc', 'def', 'ghi'])
  })

  it('split type upper', async () => {
    expect(_.name.split('abc-def-ghi', 'UPPER')).toEqual(['ABC', 'DEF', 'GHI'])
  })

  it('split type ulower', async () => {
    expect(_.name.split('abc-def-ghi', 'Ulower')).toEqual(['Abc', 'Def', 'Ghi'])
  })

  it('camel', async () => {
    expect(_.name.camel('abc-def-ghi')).toBe('abcDefGhi')
  })

  it('pascal', async () => {
    expect(_.name.pascal('abc-def-ghi')).toBe('AbcDefGhi')
  })

  it('snake', async () => {
    expect(_.name.snake('abc-def-ghi')).toBe('abc_def_ghi')
  })

  it('usnake', async () => {
    expect(_.name.usnake('abc-def-ghi')).toBe('ABC_DEF_GHI')
  })

  it('kebab', async () => {
    expect(_.name.kebab('abc-def-ghi')).toBe('abc-def-ghi')
  })

  it('train', async () => {
    expect(_.name.train('abc-def-ghi')).toBe('Abc-Def-Ghi')
  })
})
