import _ from '../../src/helper'

describe('codeHelper', () => {
  it('addIndent', () => {
    const code = `{
aaaaa

bbbbb
}`

    expect(_.code.addIndent(code, {})).toBe(`  {
  aaaaa
  
  bbbbb
  }`)

    expect(_.code.addIndent(code, { nonIndentFirstLine: true })).toBe(`{
  aaaaa
  
  bbbbb
  }`)

    expect(_.code.addIndent(code, { nonIndentLastLine: true })).toBe(`  {
  aaaaa
  
  bbbbb
}`)

    expect(_.code.addIndent(code, { nonIndentEmptyLine: true })).toBe(`  {
  aaaaa

  bbbbb
  }`)

    expect(_.code.addIndent(code, { indentStr: '    ' })).toBe(`    {
    aaaaa
    
    bbbbb
    }`)
  })

  it('generateValueCode', () => {
    expect(_.code.generateValueCode('100')).toBe("'100'")
    expect(_.code.generateValueCode(100)).toBe('100')
    expect(_.code.generateValueCode(100.05)).toBe('100.05')
    expect(_.code.generateValueCode(true)).toBe('true')
    expect(_.code.generateValueCode(null)).toBe('null')
    expect(_.code.generateValueCode(['100', 100, 100.05, true, null])).toBe("['100', 100, 100.05, true, null]")
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: false } })).toBe(`{
  a: 1,
  b: {
    c: '1',
    d: false
  }
}`)
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: false } }, { oneline: false })).toBe(`{
  a: 1,
  b: {
    c: '1',
    d: false
  }
}`)
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: false } }, { oneline: true })).toBe("{ a: 1, b: { c: '1', d: false } }")
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: false } }, { oneline: true, overwrite: { a: '2', 'b.d': "'a'.repeat(255)" } })).toBe("{ a: 2, b: { c: '1', d: 'a'.repeat(255) } }")
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: false } }, { oneline: true, overwrite: { a: '2', '*.d': "'a'.repeat(255)" } })).toBe("{ a: 2, b: { c: '1', d: 'a'.repeat(255) } }")

    // overwrite function
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: () => 'false' } }, { oneline: true })).toBe("{ a: 1, b: { c: '1', d: false } }")

    // toStr option
    expect(_.code.generateValueCode({ a: 1, b: { c: '1', d: false, e: null } }, { oneline: true, toStr: true })).toBe("{ a: '1', b: { c: '1', d: 'false', e: 'null' } }")
  })
})
