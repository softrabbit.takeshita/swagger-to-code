import _ from '../../../src/helper'

describe('swaggerHelper', () => {
  it('string', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'string'
    })).toBe('test')
  })

  it('string fomat', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'string',
      format: 'uuid'
    })).toBe('550e8400-e29b-41d4-a716-446655440000')
  })

  it('integer', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'integer'
    })).toBe(1)
  })

  it('integer minimum maximum', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'integer',
      minimum: 10,
      maximum: 20
    })).toBe(15)
  })

  it('integer minimum ', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'integer',
      minimum: 10
    })).toBe(10)
  })

  it('integer maximum ', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'integer',
      maximum: 20
    })).toBe(20)
  })

  it('integer fomat', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'integer',
      format: 'int32'
    })).toBe(4294967296)
  })

  it('number', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'number'
    })).toBe(1.0)
  })

  it('number minimum maximum', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'number',
      minimum: 10.0,
      maximum: 20.0
    })).toBe(15.0)
  })

  it('number minimum ', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'number',
      minimum: 10.0
    })).toBe(10.0)
  })

  it('number maximum ', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'number',
      maximum: 20.0
    })).toBe(20.0)
  })

  it('number fomat', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'number',
      format: 'float'
    })).toBe(1234.567)
  })

  it('boolean', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'boolean'
    })).toBe(true)
  })

  it('object', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'object',
      properties: {
        a: {
          type: 'string'
        },
        b: {
          type: 'object',
          properties: {
            c: {
              type: 'integer'
            },
            d: {
              type: 'integer'
            }
          }
        }
      }
    })).toEqual({ a: 'test', b: { c: 1, d: 1 } })
  })

  it('array', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'array',
      items: {
        type: 'string'
      }
    })).toEqual(['test'])
  })

  it('enum', () => {
    expect(_.swagger.schemaTestData({} as any, {
      type: 'string',
      enum: ['enum1', 'enum2']
    })).toBe('enum1')
  })

  it('oneOf', () => {
    expect(_.swagger.schemaTestData({} as any, {
      oneOf: [
        {
          type: 'string'
        },
        {
          type: 'integer'
        }
      ]
    })).toBe('test')
  })

  it('allOf', () => {
    expect(_.swagger.schemaTestData({} as any, {
      allOf: [
        {
          type: 'object',
          properties: {
            a: {
              type: 'string'
            }
          }
        },
        {
          type: 'object',
          properties: {
            b: {
              type: 'string'
            }
          }
        }
      ]
    })).toEqual({ a: 'test', b: 'test' })
  })
})
