import _ from '../../../src/helper'
import { Swagger, Schema, Api } from '../../../src/swagger'

const wrapper = (schema: Schema): Api => {
  return {
    requestBody: {
      content: {
        'application/json': {
          schema
        }
      }
    },
    responses: {}
  }
}

describe('requestJsonBodyFlatSchema', () => {
  it('string', () => {
    const data: Schema = {
      type: 'string'
    }
    const result = [
      {
        path: '',
        schema: data
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('object', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          type: 'string'
        }
      }
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: data.properties.a
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('object > object', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          type: 'object',
          properties: {
            b: {
              type: 'string'
            }
          }
        }
      }
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: data.properties.a

      },
      {
        path: 'a.b',
        // @ts-ignore
        schema: data.properties.a.properties.b

      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('object > array', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          type: 'array',
          items: {
            type: 'string'
          }
        }
      }
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: data.properties.a

      },
      {
        path: 'a.{n}',
        // @ts-ignore
        schema: data.properties.a.items
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('object > array > object', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              b: {
                type: 'string'
              }
            }
          }
        }
      }
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: data.properties.a

      },
      {
        path: 'a.{n}',
        // @ts-ignore
        schema: data.properties.a.items
      },
      {
        path: 'a.{n}.b',
        // @ts-ignore
        schema: data.properties.a.items.properties.b

      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('object > array > array', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          type: 'array',
          items: {
            type: 'array',
            items: {
              type: 'string'
            }
          }
        }
      }
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: data.properties.a
      },
      {
        path: 'a.{n}',
        // @ts-ignore
        schema: data.properties.a.items
      },
      {
        path: 'a.{n}.{n}',
        // @ts-ignore
        schema: data.properties.a.items.items
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('ref', () => {
    const data: Schema = {
      $ref: '#/components/schemas/SchemaA'
    }
    const api: Api = wrapper(data)
    const schemaA: Schema = {
      type: 'string'
    }
    const swagger: Swagger = {
      openapi: '3.0.0',
      paths: {
        '/': {
          get: api
        }
      },
      components: {
        schemas: {
          SchemaA: schemaA
        }
      }
    }
    const result = [
      {
        path: '',
        schema: schemaA
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema(swagger, api)).toEqual(result)
  })

  it('ref direct circulation', () => {
    const data: Schema = {
      $ref: '#/components/schemas/SchemaA'
    }
    const api: Api = wrapper(data)
    const schemaA: Schema = {
      $ref: '#/components/schemas/SchemaB'
    }
    const schemaB: Schema = {
      $ref: '#/components/schemas/SchemaA'
    }
    const swagger: Swagger = {
      openapi: '3.0.0',
      paths: {
        '/': {
          get: api
        }
      },
      components: {
        schemas: {
          SchemaA: schemaA,
          SchemaB: schemaB
        }
      }
    }
    const result = [
      {
        path: '',
        schema: schemaB // schemaAへの参照は循環しているので、打ち切られる
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema(swagger, api)).toEqual(result)
  })

  it('ref circulation', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          $ref: '#/components/schemas/SchemaA'
        }
      }
    }
    const api: Api = wrapper(data)
    const schemaA: Schema = {
      type: 'array',
      items: {
        $ref: '#/components/schemas/SchemaB'
      }
    }
    const schemaB: Schema = {
      type: 'object',
      properties: {
        b: {
          $ref: '#/components/schemas/SchemaA'
        }
      }
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: schemaA
      },
      {
        path: 'a.{n}',
        schema: schemaB
      },
      {
        path: 'a.{n}.b',
        schema: {
          $ref: '#/components/schemas/SchemaA' // schemaAへの参照は循環しているので、打ち切られる
        }
      }
    ]
    const swagger: Swagger = {
      openapi: '3.0.0',
      paths: {
        '/': {
          get: api
        }
      },
      components: {
        schemas: {
          SchemaA: schemaA,
          SchemaB: schemaB
        }
      }
    }
    expect(_.swagger.requestJsonBodyFlatSchema(swagger, api)).toEqual(result)
  })

  it('object required', () => {
    const data: Schema = {
      type: 'object',
      properties: {
        a: {
          type: 'string'
        },
        b: {
          type: 'string'
        }
      },
      required: ['a']
    }
    const result = [
      {
        path: '',
        schema: data
      },
      {
        path: 'a',
        schema: data.properties.a,
        required: true
      },
      {
        path: 'b',
        schema: data.properties.b,
        required: false
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })

  it('allOf', () => {
    const data: Schema = {
      allOf: [
        {
          type: 'object',
          properties: {
            a: {
              type: 'string',
              example: 'a'
            },
            b: {
              type: 'string',
              example: 'b'
            }
          },
          required: ['a']

        },
        {
          type: 'object',
          properties: {
            c: {
              type: 'string',
              example: 'c'
            }
          },
          required: ['c']
        }
      ]
    }
    const result = [
      {
        path: '',
        schema: {
          type: 'object',
          properties: {
            a: {
              type: 'string',
              example: 'a'
            },
            b: {
              type: 'string',
              example: 'b'
            },
            c: {
              type: 'string',
              example: 'c'
            }
          },
          required: ['a', 'c']
        }
      },
      {
        path: 'a',
        schema: {
          type: 'string',
          example: 'a'
        },
        required: true
      },
      {
        path: 'b',
        schema: {
          type: 'string',
          example: 'b'
        },
        required: false
      },
      {
        path: 'c',
        schema: {
          type: 'string',
          example: 'c'
        },
        required: true
      }
    ]
    expect(_.swagger.requestJsonBodyFlatSchema({} as any, wrapper(data))).toEqual(result)
  })
})
