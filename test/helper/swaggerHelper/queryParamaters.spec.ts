import _ from '../../../src/helper'
import { Parameter, Api } from '../../../src/swagger'

const wrapper = (parameters: Parameter[]): Api => {
  return {
    parameters,
    responses: {}
  }
}

describe('queryParamaters', () => {
  it('base', () => {
    const data: Parameter[] = [
      {
        name: 'a',
        in: 'path',
        required: true,
        schema: {
          type: 'string'
        }
      },
      {
        name: 'b',
        in: 'query',
        required: true,
        schema: {
          type: 'string'
        }
      },
      {
        name: 'c',
        in: 'path',
        required: false,
        schema: {
          type: 'string'
        }
      }
    ]
    const result = [
      {
        name: 'b',
        in: 'query',
        required: true,
        schema: {
          type: 'string'
        }
      }
    ]
    expect(_.swagger.queryParamaters({} as any, wrapper(data))).toEqual(result)
  })
})
