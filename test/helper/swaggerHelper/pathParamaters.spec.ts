import _ from '../../../src/helper'
import { Parameter, Api } from '../../../src/swagger'

const wrapper = (parameters: Parameter[]): Api => {
  return {
    parameters,
    responses: {}
  }
}

describe('pathParamaters', () => {
  it('base', () => {
    const data: Parameter[] = [
      {
        name: 'a',
        in: 'path',
        required: true,
        schema: {
          type: 'string'
        }
      },
      {
        name: 'b',
        in: 'query',
        required: true,
        schema: {
          type: 'string'
        }
      },
      {
        name: 'c',
        in: 'path',
        required: false,
        schema: {
          type: 'string'
        }
      }
    ]
    const result = [
      {
        name: 'a',
        in: 'path',
        required: true,
        schema: {
          type: 'string'
        }
      },
      {
        name: 'c',
        in: 'path',
        required: false,
        schema: {
          type: 'string'
        }
      }
    ]
    expect(_.swagger.pathParamaters({} as any, wrapper(data))).toEqual(result)
  })
})
