# 概要

指定されたSwaggerファイルを読み込んで、TypeScriptコードを生成します。（いずれ他の言語もサポートする予定なのでcodeとしています。）

現在サポートしているファイル形式はyamlのみです。

# インストール

```
yarn add -D https://gitlab.com/softrabbit.takeshita/swagger-to-code.git
```

# 生成ファイル

|生成ファイル|内容|
|---|---|
|api.ts|pathsから生成したAPI実行関数群|
|schemas.d.ts|components.schemasから生成したスキーマ定義群|
|responses.d.ts|components.responsesから生成したレスポンス定義群|

# 使い方

```
./node_modules/.bin/swagger-to-code -p ./swagger.yaml -o ./generate-code
```

|オプション|必須|意味|
|---|---|---|
|-p|必須|インプットとなるswaggerファイル（yaml形式のみ対応）|
|-o|必須|生成ファイルの書き出し先（ディレクトリパス）|
|-t|-|カスタムテンプレートの指定|

# テンプレートをカスタマイズする

### テンプレートの生成

以下のコマンドでカスタムテンプレートを作成する事ができます。

```
./node_modules/.bin/swagger-to-code-generate-templates
```

swagger-to-code-templatesディレクトリが作成されるので、テンプレート(.ejs)を編集してください。

編集が終われば以下のようにテンプレートを指定して（-t）コマンドを実行します。

```
./node_modules/.bin/swagger-to-code -p ./swagger.yaml -o ./generate-code -t ./swagger-to-code-templates
```

### テンプレートの構成

* トップレベルのディレクトリが生成するファイルに対応します。
* トップレベルディクトトリ直下のindex.ejsがエントリポイントとなります。
* _から始まるトップレベルディレクトリは対象からはずれます。
* トップレベルのディクトトリ名が、生成されるファイル名になります。
* トップレベルディクトトリ名を'|'で区切る事により生成するファイルを階層化できます。<br>また、階層名を','で区切る事により、同じテンプレートで複数のファイルを生成できます。<br>ex) 'src|app,api|schema.ts.d' => ['src/app/schema.ts.d', 'src/api/schema.ts.d']
* ディクトトリ名に'[tag]'が含まれる場合、swaggerファイルのtags分ファイルが生成されます。なお、テンプレート(ejs)からtagの変数名で対象のtag情報を参照できます。
* ディクトトリ名に'[operationId]'が含まれる場合、swaggerファイルのoperationIdが定義されているapi分ファイルが生成されます。
* ディクトトリ名に'[tag]'と'[operationId]'が含まれる場合、swaggerファイルのtags/operationId分ファイルが生成されます。

### テンプレートからアクセスできる変数

|変数名|意味|
|---|---|
|$|swaggerファイルオブジェクト|
|_|helperオブジェクト|
|options|コマンド実行時のパラメータ|
|tag|swaggerファイルのtagオブジェクト ※テンプレートディレクトリ名に'[tag]'を含む場合にのみ設定される。|
|path|swaggerファイルのpathsオブジェクトのキー<br>※テンプレートディレクトリ名に'[operationId]'を含む場合にのみ設定される。|
|method|swaggerファイルのget/post/put/delete<br>※テンプレートディレクトリ名に'[operationId]'を含む場合にのみ設定される。|
|operationId|swaggerファイルのoperationId<br>※テンプレートディレクトリ名に'[operationId]'を含む場合にのみ設定される。|