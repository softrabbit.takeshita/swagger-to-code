import fs from 'fs'
import yaml from 'js-yaml'
import ejs from 'ejs'
import { CommandOptions } from './type.d'
import helper from './helper'
import path from 'path'
import { Swagger } from './swagger.d'

// const eslintCli = require(path.resolve(__dirname, '../node_modules/eslint/lib/cli.js'))
// const eslintrcForTsPath = path.resolve(__dirname, '../eslintrc/typescript.eslintrc.js')

export function main (options: CommandOptions) {
  const inputSwagger = path.resolve(process.cwd(), options.path)
  const outputDir = path.resolve(process.cwd(), options.output)
  const templates = options.templates || path.resolve(__dirname, '../templates')

  const swagger = yaml.load(fs.readFileSync(inputSwagger, { encoding: 'utf-8' })) as Swagger

  // @ts-ignore
  const swaggerTags: {name: string}[] = Array.isArray(swagger.tags) ? swagger.tags : []

  const data = {
    $: swagger,
    _: helper,
    options
  }

  for (const topLevelDirName of fs.readdirSync(templates)) {
    if (topLevelDirName.startsWith('_')) {
      continue
    }
    const template = path.resolve(templates, topLevelDirName + '/index.ejs')
    const outputPaths = resolveOutputPaths(topLevelDirName)

    for (const outputPath of outputPaths) {
      if (outputPath.includes('[tag]') && outputPath.includes('[operationId]')) {
        // tags loop
        for (const tag of swaggerTags) {
          // operationId loop
          for (const operation of getSwaggerTagOprations(swagger, tag.name)) {
            const output = path.resolve(outputDir, outputPath.replace(/\[tag\]/g, tag.name).replace(/\[operationId\]/g, operation.id))
            genarate(template, { ...data, tag, path: operation.path, method: operation.method, operationId: operation.id }, output)
          }
        }
      } else if (outputPath.includes('[tag]')) {
        // tags loop
        for (const tag of swaggerTags) {
          const output = path.resolve(outputDir, outputPath.replace(/\[tag\]/g, tag.name))
          genarate(template, { ...data, tag }, output)
        }
      } else if (outputPath.includes('[operationId]')) {
        // operationId loop
        for (const operation of getSwaggerTagOprations(swagger)) {
          const output = path.resolve(outputDir, outputPath.replace(/\[operationId\]/g, operation.id))
          genarate(template, { ...data, path: operation.path, method: operation.method, operationId: operation.id }, output)
        }
      } else {
        const output = path.resolve(outputDir, outputPath)
        genarate(template, data, output)
      }
    }
  }
}

type Operation = {
  path:string
  method: string
  id: string
}
function getSwaggerTagOprations (swagger: any, tag: string | null = null) {
  const operations: Operation[] = []
  for (const path of Object.getOwnPropertyNames(swagger.paths)) {
    for (const method of Object.getOwnPropertyNames(swagger.paths[path])) {
      const api = swagger.paths[path][method]
      if (tag !== null && tag !== api.tags[0]) {
        continue
      }
      if (api.operationId) {
        operations.push({
          path,
          method,
          id: api.operationId
        })
      }
    }
  }
  return operations
}

function resolveOutputPaths (topLevelDirName: string) {
  let outputPaths = ['.']

  const hierarchys = topLevelDirName.split('|')
  for (const hierarchy of hierarchys) {
    const nextOutputPaths = []

    const names = hierarchy.split(',')
    for (const name of names) {
      for (const outputPath of outputPaths) {
        nextOutputPaths.push(`${outputPath}/${name}`)
      }
    }
    outputPaths = nextOutputPaths
  }
  return outputPaths
}

function genarate (template: string, data: any, output: string) {
  const ejsOptions = {
  }

  ejs.renderFile(template, data, ejsOptions, (error, renderText) => {
    if (error) {
      throw error
    }

    const _dir = path.dirname(output)
    if (!fs.existsSync(_dir)) {
      fs.mkdirSync(_dir, { recursive: true })
    }
    fs.writeFileSync(output, renderText)

    // eslintで整形
    // eslintCli.execute([
    //   '/usr/local/bin/node', // おそらくあまり関係ない
    //   '../node_modules/.bin/eslint', // おそらくあまり関係ない
    //   '--fix',
    //   ...(output.match(/\.ts$/) ? ['--config', eslintrcForTsPath] : []),
    //   output
    // ], null)

    console.log(`✨✨✨ genarate ${output}  ✨✨✨`)
  })
}
