#!/usr/bin/env node
import { Command } from 'commander'
import { version, name as packageName } from '../package.json'
import { main } from './main'
import { CommandOptions } from './type.d'

const command = new Command(packageName)

command.storeOptionsAsProperties(true)

command
  .version(version, '-v, --version', 'output the current version')
  .description('Generate code via swagger scheme.\nSupports OA 3.0, 2.0, JSON, yaml.')

command
  .requiredOption('-p, --path <path>', 'path to swagger scheme')
  .requiredOption('-o, --output <output>', 'output directory path of genarate file')
  .option('-t, --templates <path>', 'path to template directory')

command.parse(process.argv)

const options: CommandOptions = command as any

main(options)
