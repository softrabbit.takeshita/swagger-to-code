#!/usr/bin/env node
import path from 'path'
import fsExtra from 'fs-extra'

const src = path.resolve(__dirname, '../templates')
const dist = path.resolve(process.cwd(), './swagger-to-code-templates')

fsExtra.copySync(src, dist)
