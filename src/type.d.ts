export interface CommandOptions {
  path: string
  output: string
  templates?: string
}
