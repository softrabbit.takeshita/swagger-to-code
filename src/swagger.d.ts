export type Server = {
  url: string
}

export type Tag = {
  name: string
  description: string
}

export type Ref = {
  $ref: string
}

export type StringSchema = {
  type: 'string'
  format?: 'date' | 'date-time' | 'password' | 'byte' | 'binary' | 'email' | 'uuid' | 'uri' | 'hostname' | 'ipv4' | 'ipv6'
  minLength?: number,
  maxLength?: number,
  enum?: string[]
  example?: string
  nullable?: boolean
}

export type IntegerSchema = {
  type: 'integer'
  format?: 'int32' | 'int64'
  minimum?: number
  maximum?: number
  enum?: number[]
  example?: number
  nullable?: boolean
}

export type NumberSchema = {
  type: 'number'
  format?: 'float' | 'double'
  minimum?: number
  maximum?: number
  enum?: number[]
  example?: number
  nullable?: boolean
}

export type BooleanSchema = {
  type: 'boolean'
  example?: boolean
  nullable?: boolean
}

export type Schema = StringSchema | IntegerSchema | NumberSchema | BooleanSchema | {
  type: 'object'
  properties: {
    [keyof: string]: Schema
  }
  required?: string[]
  nullable?: boolean
} | {
  type: 'array'
  items: Schema
  uniqueItems?: boolean
  nullable?: boolean
} | {
  allOf: Schema[]
} | {
  oneOf: Schema[]
} | Ref

export type Parameter = {
  name: string
  in: 'path' | 'query'
  required?: boolean
  description?: string
  schema: Schema
}

export type Header = {
  schema: Schema
  description?: string
}

export type Response = {
  description?: string
  headers?: {
    [keyof: string]: Header
  }
  content: {
    ['application/json']: {
      schema: Schema
    }
  }
} | Ref

export type Info = {
  description?: string
  version: '1.0.0' | string
  title?: string
  contact?: {
    email: string
  }
  license?: {
    name: 'MIT' | string
    url: string
  }
}

export type SecurityItem = {
  AccessTokenAuth: []
}

export type RequestBody = {
  description?: string
  required?: boolean
  content: {
    'application/json': {
      schema: Schema
    }
  }
}

export type Api = {
  tags?: string[]
  summary?: string
  description?: string
  operationId?: string
  security?: SecurityItem[]
  parameters?: Parameter[]
  requestBody?: RequestBody
  responses: {
    [keyof: number]: Response
  }
}

export type Swagger = {
  openapi: '3.0.0' | string
  info?: Info
  servers?: Server[]
  tags?: Tag[]
  paths: {
    [keyof: string]: {
      get?: Api
      post?: Api
      put?: Api
      delete?: Api
    }
  }
  components: {
    securitySchemes?: {
      AccessTokenAuth: {
        type: 'apiKey' | string
        in: 'header' | string
        name: 'X-Access-Token' | string
      }
    }
    responses?: {
      [keyof: string]: Response
    }
    schemas?: {
      [keyof: string]: Schema
    }
  }
}
