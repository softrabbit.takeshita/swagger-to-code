import { Swagger, Schema, StringSchema, IntegerSchema, NumberSchema, BooleanSchema, Response, Api } from '../swagger.d'

export function resolveRef (swagger: Swagger, ref: string) {
  if (!ref.startsWith('#/')) {
    throw new Error(`can not resolve $ref "${ref}"`)
  }

  let obj = swagger
  for (const key of ref.slice(2).split('/')) {
    // @ts-ignore
    obj = obj[key]
    if (obj === undefined) {
      throw new Error(`can not resolve $ref "${ref}"`)
    }
  }

  // @ts-ignore
  return obj as Schema | Response
}

export function convertPath (path: string, type: 'ts' | 'express') {
  switch (type) {
    case 'ts': {
      const _path = path.replace(/\{.+?\}/g, '$$$&')
      return _path === path ? "'" + _path + "'" : '`' + _path + '`'
    }
    case 'express': {
      const _path = path.replace(/\{(.+?)\}/g, ':$1')
      return "'" + _path + "'"
    }
    default: {
      throw new Error('unsuppert type')
    }
  }
}

function stringTestData (schema: StringSchema) {
  const formatTestData = {
    date: '2017-07-21',
    'date-time': '2017-07-21T17:32:28Z',
    password: 'password',
    byte: 'U3dhZ2dlciByb2Nrcw==',
    binary: '',
    email: 'test@example.com',
    uuid: '550e8400-e29b-41d4-a716-446655440000',
    uri: 'http://example.com',
    hostname: 'hostname',
    ipv4: '192.168.0.1',
    ipv6: '2001:db8::'
  }
  if (schema.format !== undefined) {
    return formatTestData[schema.format]
  } else if (schema.minLength !== undefined && schema.maxLength !== undefined) {
    return 'a'.repeat(Math.floor((schema.minLength + schema.maxLength) / 2))
  } else if (schema.minLength !== undefined) {
    return 'a'.repeat(schema.minLength)
  } else if (schema.maxLength !== undefined) {
    return 'a'.repeat(schema.maxLength)
  } else {
    return 'test'
  }
}

function integerTestData (schema: IntegerSchema) {
  const formatTestData = {
    int32: 4294967296,
    int64: 18446744073709551616
  }
  if (schema.minimum !== undefined && schema.maximum !== undefined) {
    return Math.floor((schema.minimum + schema.maximum) / 2) // 代表値
  } else if (schema.minimum !== undefined) {
    return schema.minimum
  } else if (schema.maximum !== undefined) {
    return schema.maximum
  } else if (schema.format !== undefined) {
    return formatTestData[schema.format]
  } else {
    return 1
  }
}

function numberTestData (schema: NumberSchema) {
  const formatTestData = {
    float: 1234.567,
    double: 1234567.8901234
  }
  if (schema.minimum !== undefined && schema.maximum !== undefined) {
    return (schema.minimum + schema.maximum) / 2 // 代表値
  } else if (schema.minimum !== undefined) {
    return schema.minimum
  } else if (schema.maximum !== undefined) {
    return schema.maximum
  } else if (schema.format !== undefined) {
    return formatTestData[schema.format]
  } else {
    return 1.0
  }
}

function booleanTestData (schema: BooleanSchema) {
  return true
}

export function schemaTestData (swagger: Swagger, schema: Schema): any {
  if ('$ref' in schema) {
    schema = resolveRef(swagger, schema.$ref) as Schema
  }

  if ('example' in schema) {
    // example
    return schema.example
  } else if ('allOf' in schema) {
    // allOf
    const _schema = mergeAllOfSchema(swagger, schema.allOf)
    return schemaTestData(swagger, _schema)
  } else if ('oneOf' in schema) {
    // oneOf
    return schemaTestData(swagger, schema.oneOf[0])
  } else if ('enum' in schema && schema.enum) {
    // enum
    return schema.enum[0]
  } else if ('type' in schema && schema.type !== undefined) {
    switch (schema.type) {
      case 'string':
        return stringTestData(schema)
      case 'integer':
        return integerTestData(schema)
      case 'number':
        return numberTestData(schema)
      case 'boolean':
        return booleanTestData(schema)
      case 'object': {
        const obj: {[keyof: string]: any} = {}
        for (const propertieName of Object.getOwnPropertyNames(schema.properties)) {
          obj[propertieName] = schemaTestData(swagger, schema.properties[propertieName])
        }
        return obj
      }
      case 'array': {
        return [schemaTestData(swagger, schema.items)]
      }
    }
  }
  throw new Error(`schema analize error ${JSON.stringify(schema)}`)
}

export function generatePathTestData (swagger: Swagger, api: Api) {
  const result: {[keyof: string]: Schema} = {}

  ;(api.parameters || [])
    .filter(parameter => parameter.in === 'path')
    .forEach(parameter => {
      result[parameter.name] = schemaTestData(swagger, parameter.schema)
    })

  return result
}

export function generateQueryTestData (swagger: Swagger, api: Api) {
  const result: {[keyof: string]: Schema} = {}

  ;(api.parameters || [])
    .filter(parameter => parameter.in === 'query')
    .forEach(parameter => {
      result[parameter.name] = schemaTestData(swagger, parameter.schema)
    })

  return result
}

export function generateJsonBodyTestData (swagger: Swagger, api: Api) {
  const schema = api.requestBody?.content['application/json'].schema
  return schema ? schemaTestData(swagger, schema) : {}
}

export function pathParamaters (swagger: Swagger, api: Api) {
  return (api.parameters || [])
    .filter(parameter => parameter.in === 'path')
    .map(parameter => {
      return {
        ...parameter,
        schema: '$ref' in parameter.schema ? resolveRef(swagger, parameter.schema.$ref) : parameter.schema
      }
    })
}

export function queryParamaters (swagger: Swagger, api: Api) {
  return (api.parameters || [])
    .filter(parameter => parameter.in === 'query')
    .map(parameter => {
      return {
        ...parameter,
        schema: '$ref' in parameter.schema ? resolveRef(swagger, parameter.schema.$ref) : parameter.schema
      }
    })
}

export function requestJsonBodyFlatSchema (swagger: Swagger, api: Api) {
  const schema = api.requestBody?.content['application/json'].schema
  return schema ? flatSchema(swagger, schema) : []
}

function mergeAllOfSchema (swagger: Swagger, allOf: Schema[]): Schema {
  const properties = {}
  let required: string[] = []
  allOf.forEach(s => {
    const _schema = '$ref' in s ? resolveRef(swagger, s.$ref) : s
    if (!('type' in _schema) || _schema.type !== 'object') {
      throw new Error()
    }
    Object.assign(properties, _schema.properties)
    required = required.concat(_schema.required || [])
  })
  return {
    type: 'object',
    properties: properties,
    required: required
  }
}

type SchemaInfo = {
  path: string,
  schema: Schema,
  required?: boolean
}

export function flatSchema (swagger: Swagger, schema: Schema) {
  const f = (schema: Schema, required: boolean | undefined = undefined, path: string = '', refs: string[] = []): SchemaInfo[] => {
    refs = refs.slice()
    if ('$ref' in schema && !refs.includes(schema.$ref) /* 循環参照禁止 */) {
      refs.push(schema.$ref)
      return f(resolveRef(swagger, schema.$ref) as Schema, required, path, refs)
    } else if ('oneOf' in schema) {
      return f(schema.oneOf[0], required, path, refs)
    } else if ('allOf' in schema) {
      return f(mergeAllOfSchema(swagger, schema.allOf), required, path, refs)
    } else {
      let result: SchemaInfo[] = [{
        path: path,
        schema: schema,
        required: required
      }]

      if ('type' in schema && schema.type === 'object') {
        for (const propertieName of Object.getOwnPropertyNames(schema.properties)) {
          result = result.concat(f(
            schema.properties[propertieName],
            schema.required?.includes(propertieName), // undefined or boolean
            path ? `${path}.${propertieName}` : propertieName,
            refs
          ))
        }
      } else if ('type' in schema && schema.type === 'array') {
        result = result.concat(f(
          schema.items,
          undefined,
          path ? `${path}.{n}` : '{n}',
          refs
        ))
      }
      return result
    }
  }

  return f(schema)
}
