type AddIndentOptions = {
  indentStr?: string,
  nonIndentFirstLine?: boolean,
  nonIndentLastLine?: boolean,
  nonIndentEmptyLine?: boolean
}

export function addIndent (code: string, options: AddIndentOptions = {}) {
  const {
    indentStr = '  ',
    nonIndentFirstLine = false,
    nonIndentLastLine = false,
    nonIndentEmptyLine = false
  } = options

  const lines = code.split('\n')

  return lines.map((line, lineNum) => {
    const noIndent =
        // first line and nonIndentFirstLine
        (nonIndentFirstLine && lineNum === 0) ||
        // last line and nonIndentLastLine
        (nonIndentLastLine && lineNum === lines.length - 1) ||
        // empty line and nonIndentEmptyLine
        (nonIndentEmptyLine && line === '')

    return noIndent ? line : indentStr + line
  }).join('\n')
}

type GenerateValueCodeOption = {
  oneline?: boolean
  toStr?: boolean
  overwrite?: {[keyof: string]: string}
}
function matchOverwite (ref: string, overwrite: {[keyof: string]: string}) {
  const _ref = ref.split('.')

  const match = Object.keys(overwrite).find(target => {
    const _target = target.split('.')
    if (_ref.length !== _target.length) {
      return false
    }
    for (let i = 0; i < _ref.length; i++) {
      if (_target[i] !== '*' && _ref[i] !== _target[i]) {
        return false
      }
    }
    return true
  })
  return match === undefined ? null : overwrite[match]
}

export function generateValueCode (val: any, option: GenerateValueCodeOption = {}): string {
  const {
    oneline = false,
    toStr = false,
    overwrite = {}
  } = option

  const f = (val: any, ref: string = ''): string => {
    const overwiteCode = matchOverwite(ref, overwrite)
    if (overwiteCode !== null) {
      return overwiteCode
    }

    switch (typeof val) {
      case 'string':
        return `'${val}'`
      case 'number':
        if (toStr) {
          return `'${val}'`
        } else {
          return val.toString()
        }
      case 'boolean':
        if (toStr) {
          return val ? "'true'" : "'false'"
        } else {
          return val ? 'true' : 'false'
        }
      case 'object':
        if (val === null) {
          if (toStr) {
            return "'null'"
          } else {
            return 'null'
          }
        } else if (Array.isArray(val)) {
          return '[' + val.map((v, i) => f(v, ref ? `${ref}.${i}` : i.toString())).join(', ') + ']'
        } else {
          const code = Object.getOwnPropertyNames(val)
            .map(propertieName => {
              const _name = propertieName.match(/^[a-zA-Z0-9]+$/) ? propertieName : `'${propertieName}'`
              const _val = f(val[propertieName], ref ? `${ref}.${propertieName}` : propertieName)
              return _name + ': ' + _val
            }).join(oneline ? ', ' : ',\n')
          return oneline ? '{ ' + code + ' }' : '{\n' + addIndent(code, { indentStr: '  ', nonIndentEmptyLine: true }) + '\n}'
        }
      case 'function':
        return val()
      default:
        throw new Error(`generateValueCode error ${val}`)
    }
  }

  return f(val)
}
