function firstChatToUpperCase (str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

function firstChatToLowerCase (str: string) {
  return str.charAt(0).toLowerCase() + str.slice(1)
}

export function split (str: string, type: 'lower' | 'UPPER' | 'Ulower'): string[] {
  let words
  if (str.includes('-')) {
    words = str.split('-')
  } else if (str.includes('_')) {
    words = str.split('_')
  } else if (str.match(/^[A-Z]+$/)) {
    // XXX
    words = [str]
  } else {
    let substr = str
    let match
    const _words = []
    while ((match = substr.match(/^([A-Za-z][a-z0-9]*)/)) !== null) {
      _words.push(match[0])
      substr = substr.slice(match[0].length)
    }
    if (_words.length > 0) {
      words = _words
    } else {
      words = [str] // unknown
    }
  }

  switch (type) {
    case 'lower':
      return words.map(w => w.toLowerCase())
    case 'UPPER':
      return words.map(w => w.toUpperCase())
    case 'Ulower':
      return words.map(w => firstChatToUpperCase(w.toLowerCase()))
    default:
      throw new Error()
  }
}

export function camel (str: string) {
  const words = split(str, 'Ulower')
  return firstChatToLowerCase(words.join('')) // camelCase
}

export function pascal (str: string) {
  const words = split(str, 'Ulower')
  return words.join('') // PascalCase
}

export function snake (str: string) {
  const words = split(str, 'lower')
  return words.join('_') // snake_case
}

export function usnake (str: string) {
  const words = split(str, 'UPPER')
  return words.join('_') // USNAKE_CASE
}

export function kebab (str: string) {
  const words = split(str, 'lower')
  return words.join('-') // kebab-case
}

export function train (str: string) {
  const words = split(str, 'Ulower')
  return words.join('-') // Train-Case
}
