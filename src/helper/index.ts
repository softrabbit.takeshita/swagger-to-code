import * as nameHelper from './nameHelper'
import * as swaggerHelper from './swaggerHelper'
import * as codeHelper from './codeHelper'

export default {
  // 変数名加工関数郡
  name: nameHelper,
  // Swaggerファイル解析関数郡
  swagger: swaggerHelper,
  // コード加工関数郡
  code: codeHelper
}
